public class MergeSort {
  int[] Array;
  int size;

  public MergeSort(int[] Array, int size) {
    this.Array = Array;
    this.size = size;
  }

  public void printArray() {
    for (int i = 0; i < size; i++) {
      System.out.print(this.Array[i] + " ");
    }
    System.out.println("");
  }

  public void MergeSort_(int pivot, int lastIndex) {
    if (pivot < lastIndex) {
      int midIndex = (int) Math.floor((pivot + lastIndex)/2);
      this.MergeSort_(pivot, midIndex);
      this.MergeSort_(midIndex + 1, lastIndex);
      this.merge(pivot, midIndex, lastIndex);
    }
  }
  public void merge(int pivot, int midIndex, int lastIndex) {
    int i, j;
    int sizeL = midIndex - pivot + 1;
    int sizeR = lastIndex - midIndex;
    int[] leftSide = new int[sizeL + 1];
    int[] rightSide = new int[sizeR + 1];
    for (i = 1; i < sizeL; i++) {
      leftSide[i] = this.Array[pivot + i - 1];
    }
    for (j = 1; j < sizeR; j++) {
      rightSide[j] = this.Array[midIndex + j];
    }
    leftSide[sizeL] = 99999999;
    rightSide[sizeR] = 99999999;
    i = 0; j =0;
    for (int k = 0; k < lastIndex; k++) {
      if (leftSide[i] <= rightSide[j]) {
        this.Array[k] = leftSide[i]; i++;
      } else {
        this.Array[k] = rightSide[j]; j++;
      }
    }
  }
  public static void main(String[] args) {
    int[] in = {5, 2, 4, 3, 1};
    MergeSort merge = new MergeSort(in, 5);
    merge.MergeSort_(0, 5);
    merge.printArray();
  }
}
